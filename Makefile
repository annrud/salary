API_CONTAINER := api

ifeq (exec, $(firstword $(MAKECMDGOALS)))
  EXEC_ARGS := $(wordlist 2, $(words $(MAKECMDGOALS)), $(MAKECMDGOALS))
  $(eval $(EXEC_ARGS):;@true)
endif

ifeq (logs, $(firstword $(MAKECMDGOALS)))
  LOGS_ARGS := $(wordlist 2, $(words $(MAKECMDGOALS)), $(MAKECMDGOALS))
  $(eval $(EXEC_ARGS):;@true)
endif

build:
	@docker compose build --no-cache

up:
	@docker compose up -d

down:
	@docker compose down

start:
	@docker compose start

stop:
	@docker compose stop

restart:
	@docker compose restart $(API_CONTAINER)

ps:
	@docker compose ps

logs:
	@docker compose logs $(LOGS_ARGS)

exec:
	@docker compose exec $(EXEC_ARGS)
