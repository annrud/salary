from datetime import timedelta
from typing import Annotated

from fastapi import APIRouter, Body, HTTPException, status
from app.auth.schemas import LoginRequest, LoginResponse
from app.auth.service import (ACCESS_TOKEN_EXPIRE_MINUTES, authenticate,
                              create_access_token)

__all__ = ("login", "router",)

router = APIRouter()


@router.post('/login')
async def login(form_data: Annotated[
    LoginRequest,
    Body(
        examples=[
            {
                "login": "test",
                "password": "test",
            }
        ],
    ),
]) -> LoginResponse:
    user = await authenticate(form_data.login, form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect login or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(
        data={"sub": user.login}, expires_delta=access_token_expires
    )

    return LoginResponse(access_token=access_token, token_type="bearer")
