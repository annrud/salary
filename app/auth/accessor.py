from sqlalchemy import select
from sqlalchemy.orm import selectinload

from app.auth.models import User
from app.storage.database import async_session

__all__ = ("find_user_by_login",)


async def find_user_by_login(login: str) -> User | None:
    async with async_session() as session:
        result = await session.execute(
            select(User)
            .filter(User.login == login)
            .options(
                selectinload(User.employee)
            )
        )

        return result.scalar_one_or_none()
