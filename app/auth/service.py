import os
from datetime import datetime, timedelta, timezone
from typing import Annotated

import jwt
from dotenv import find_dotenv, load_dotenv
from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer
from jwt.exceptions import InvalidTokenError
from passlib.context import CryptContext

from app.auth.accessor import find_user_by_login
from app.auth.models import User

__all__ = (
    "verify_password",
    "create_access_token",
    "get_current_user",
    "authenticate",
    "ACCESS_TOKEN_EXPIRE_MINUTES",
)


load_dotenv(find_dotenv())

SECRET_KEY = os.getenv("SECRET_KEY")
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 30

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="login")


def verify_password(plain, hashed) -> str:
    return pwd_context.verify(plain, hashed)


def get_password_hash(password) -> str:
    return pwd_context.hash(password)


async def authenticate(username, password) -> User | None:
    user = await find_user_by_login(username)
    if not user:
        return None
    if not verify_password(password, user.hashed_password):
        return None

    return user


def create_access_token(
        data: dict, expires_delta: timedelta | None = None
) -> str:
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.now(timezone.utc) + expires_delta
    else:
        expire = datetime.now(timezone.utc) + timedelta(minutes=15)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)

    return encoded_jwt


async def get_current_user(
        token: Annotated[str, Depends(oauth2_scheme)]
) -> User | None:
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        login: str = payload.get("sub")
        if login is None:
            raise credentials_exception
    except InvalidTokenError:
        raise credentials_exception
    user = await find_user_by_login(login=login)
    if user is None:
        raise credentials_exception
    return user
