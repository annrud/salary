from typing import TYPE_CHECKING

from sqlalchemy.orm import Mapped, mapped_column, relationship

from app.storage.base import Base

__all__ = ("User",)


if TYPE_CHECKING:
    from app.employee.models import Employee


class User(Base):
    __tablename__ = "user"

    id: Mapped[int] = mapped_column(primary_key=True)
    login: Mapped[str] = mapped_column(nullable=False, unique=True, index=True)
    hashed_password: Mapped[str] = mapped_column(nullable=False)
    employee: Mapped["Employee"] = relationship(
        "Employee", back_populates="user"
    )
