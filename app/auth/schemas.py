from fastapi.param_functions import Annotated, Body, Doc
from pydantic import BaseModel


class LoginRequest(BaseModel):
    login: Annotated[
        str,
        Body(),
        Doc(
            """
            `login` string. The OAuth2 spec requires the exact field name
            `login`.
            """
        )
    ]
    password: Annotated[
        str,
        Body(),
        Doc(
            """
            `password` string. The OAuth2 spec requires the exact field name
            `password`.
            """
        )
    ]


class LoginResponse(BaseModel):
    access_token: str
    token_type: str
