from typing import TYPE_CHECKING, Annotated

from fastapi import APIRouter, Depends

from app.auth.service import get_current_user

__all__ = ("salary", "router",)

router = APIRouter()

if TYPE_CHECKING:
    from app.auth.models import User


@router.get('/salary')
async def salary(
    current_user: Annotated["User", Depends(get_current_user)],
):
    return {
        "salary": current_user.employee.salary,
        "increase_date": current_user.employee.increase_date,
    }
