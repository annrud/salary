from datetime import datetime
from typing import TYPE_CHECKING

from sqlalchemy import ForeignKey
from sqlalchemy.orm import Mapped, mapped_column, relationship

from app.storage.base import Base

__all__ = ("Employee", )


if TYPE_CHECKING:
    from app.auth.models import User


class Employee(Base):
    __tablename__ = "employee"

    id: Mapped[int] = mapped_column(primary_key=True)
    user_id: Mapped[int] = mapped_column(
        ForeignKey("user.id", ondelete="RESTRICT"), nullable=False
    )
    user: Mapped["User"] = relationship("User", back_populates="employee")
    salary: Mapped[float] = mapped_column(default=0)
    increase_date: Mapped[datetime] = mapped_column(default=None)
