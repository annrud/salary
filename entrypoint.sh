#! /bin/sh
cp -n .env.dist .env
python3.12 -m alembic upgrade head
exec uvicorn main:app --host 0.0.0.0 --port 8080 --reload
