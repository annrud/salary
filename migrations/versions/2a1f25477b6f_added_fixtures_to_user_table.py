"""Added fixtures

Revision ID: 2a1f25477b6f
Revises: 4b0b3811f5d2
Create Date: 2024-05-23 09:46:46.550747

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


data = [
    {"login": "test", "hashed_password": "$2b$12$DqdP/c7p7v6iGzKSeSiKte8Gcz3wwiQEsDbiVsyxjnZp6LZfMUdKC"},
    {"login": "test1", "hashed_password": "$2b$12$3Z.9q0qQyNn9C7w3vXr3gO1OjFhQUBuB435mSGXRcT93pYjHuXb3C"},
    {"login": "test2", "hashed_password": "$2b$12$01EcsRWjSNwi75JVZjha.esIuhsD5AhBm5i.AtEFaDlfi9elb7mNe"},
]
# revision identifiers, used by Alembic.
revision: str = '2a1f25477b6f'
down_revision: Union[str, None] = '4b0b3811f5d2'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = '4b0b3811f5d2'


def upgrade() -> None:
    conn = op.get_bind()
    metadata = sa.MetaData()
    user = sa.Table(
        "user",
        metadata,
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("login", sa.String),
        sa.Column("hashed_password", sa.String),
    )

    conn.execute(
        user.insert(),
        data
    )


def downgrade() -> None:
    op.execute('DELETE FROM "user" WHERE login IN (\'test\', \'test1\', \'test2\')')
