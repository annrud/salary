"""Added fixtures to employee table

Revision ID: 760bbf617ffc
Revises: 2a1f25477b6f
Create Date: 2024-05-23 10:20:16.374014

"""
from typing import Sequence, Union
from datetime import datetime
from alembic import op
import sqlalchemy as sa

data = [
    {"user_id": 1, "salary": 120000.0, "increase_date": "2024-05-23 18:23:03.000000"},
    {"user_id": 2, "salary": 150520.0, "increase_date": "2024-10-10 18:23:03.000000"},
    {"user_id": 3, "salary": 20000.0, "increase_date": "2024-06-15 18:23:03.000000"},
]

# revision identifiers, used by Alembic.
revision: str = '760bbf617ffc'
down_revision: Union[str, None] = '2a1f25477b6f'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = '2a1f25477b6f'

for item in data:
    item["increase_date"] = datetime.strptime(item["increase_date"], "%Y-%m-%d %H:%M:%S.%f")


def upgrade() -> None:
    conn = op.get_bind()
    metadata = sa.MetaData()
    employee = sa.Table(
        "employee",
        metadata,
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("user_id", sa.Integer),
        sa.Column("salary", sa.Float),
        sa.Column("increase_date", sa.DateTime),
    )

    conn.execute(
        employee.insert(),
        data
    )


def downgrade() -> None:
    op.execute('DELETE FROM employee WHERE user_id IN (7, 8, 9)')
