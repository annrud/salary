FROM python:3.12.3-alpine

WORKDIR /code

COPY . .

RUN set -eux \
    && apk add --no-cache \
        gcc \
        python3-dev \
        musl-dev \
        linux-headers \
    && chmod u+x entrypoint.sh \
    && python3.12 -m pip install --upgrade pip \
    && pip3 install -r requirements.txt


ENTRYPOINT ["sh", "entrypoint.sh"]
