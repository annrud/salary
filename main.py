
from fastapi import FastAPI

from app.auth.router import router as auth_router
from app.employee.router import router as employee_router
import uvicorn

app = FastAPI()

app.include_router(auth_router)
app.include_router(employee_router)


# if __name__ == "__main__":
#     uvicorn.run(app, host='0.0.0.0', port=8080, reload=True)
